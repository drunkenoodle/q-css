(function(){


	// Load on ready
	document.addEventListener("DOMContentLoaded", function() {

		var ToggleContent = function(el, activator){

			var self = this;

			this.cachedHeight = 0;
			this.activator = activator;
			this.activator.className = "activator"

			this.el = el;
			this.openState = -1;
			this.childContent = this.getChildContent();
			this.cachedHeight = 0;

			this.el.className += " toggle-enabled";
			this.el.parentNode.insertBefore(activator, el);

			activator.innerHTML = "Open";
			activator.addEventListener("click", function()
			{
				self.handleClick.call(self);
			});

			window.addEventListener("resize", function()
			{
				if(self.openState > 0)
					self.el.style.height = self.getHeightFor(self.childContent) + "px";
			});

			this.handleClick(1);

		}

		ToggleContent.prototype.getChildContent = function()
		{

			var nodes = this.el.childNodes, i = 0, len = nodes.length, node;
			for(i; i < len; i++)
			{
				if(nodes[i].className === "toggle-content")
					node = nodes[i];
			}

			return node;

		}

		ToggleContent.prototype.getHeightFor = function(el)
		{
			
			return el.clientHeight;

		}

		ToggleContent.prototype.handleClick = function(n)
		{

			this.openState *= n || -1;

			this.el.style.height = this.openState > 0 ? this.getHeightFor(this.childContent) + "px" : 0 + "px";
			this.activator.innerHTML = this.openState > 0 ? "Close" : "Open";

		}

		var toggledElements = document.getElementsByClassName("can-toggle");
		var i = 0, len = toggledElements.length;

		if(toggledElements.length > 0) {
			
			for(i; i < len; i++)
			{
				new ToggleContent(toggledElements[i], document.createElement("button"));
			}

		}

	});

})();